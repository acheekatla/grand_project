const express = require("express");
const mongoose = require("mongoose");
const app = express();
const { MONGO_URL, PORT } = require("./constants");

const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const user = require("./routes/user_register");
app.use("/", user);

mongoose.connect(MONGO_URL, () => {
  // on success of connectionl try listening on PORT
  app.listen(PORT, () => {
    console.log(
      "Database connection is Ready and " + "Server is Listening on Port ",
      PORT
    );
  });
});
