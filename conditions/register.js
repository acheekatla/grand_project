/**
 *conditions for registration page
 */
const{user}=require("../database/model/u_index")
exports.user_register = function (req, res) {
  const { username, phone, email, password ,nationality} = req.body;
  user.findOne({ email: email }, function (err, data) {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      let person = new user({
        username: username,
        phone: phone,
        email: email,
        password: password,
        nationality:nationality
      });
      person.save(function (err) {
        if (err) {
          return res.status(500);
        }
        res
          .status(201)
          .send({ data: person, message: "Registered Successfully" });
      });
    } else {
      res.status(226).send({ message: "User already exist" });
    }
  });
};

