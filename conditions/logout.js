/**
 * logout condition
 */

const { user } = require("../database/model/u_index");

exports.user_logout = function (req, res) {
  const {username}=req.body;
  user.find(username,function (err) {
    if (err) {
        return res.status(500);
    }
    res.status(201).send({ "message":"session expired"})
});}