/**
 * list of movies
 */
const {movie} = require("../database/model/movies");

exports.movies_list = function (req, res) {
  const { title, genre, release_date } = req.body;
  movie.find({}, (err, data) => {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      let movies = new movie({
        title: title,
        genre: genre,
        release_date: release_date,
      });
      movies.save(function (err) {
        if (err) {
          return res.status(500);
        }
        res.status(201).send({ data: movies, message: "movies are here" });
      });
    }

    res.status(200).send();
  });
};
