/**
 * conditions for a login page
 */
const { user } = require("../database/model/u_index");
exports.user_login = function (req, res) {
  const { name, email, password } = req.body;
  user.findOne({ email: email }, function (err, data) {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      res.status(404).send({
        name: "true",
        email: "invalid",
        password: "false",
        status: "inactive",
        message: "email not found",
      });
    } else {
      user.findOne(
        ({ email: email }, { password: password }),
        function (err, data2) {
          if (err) {
            return res.status(500);
          }
          if (data2 == null) {
            res.status(401).send({
              email: email,
              password: "false",
              status: "inactive",
              message: "wrong password",
            });
          } else {
            res.status(200).send({
              userId: data2._id,
              name: data2.name,
              email: email,
              password: "true",
              status: "active",
              loginTime: new Date(),
              message: "Login successful",
            });
          }
        }
      );
    }
  });
};
