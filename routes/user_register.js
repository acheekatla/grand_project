const express = require('express');
const router = express.Router();
const login = require('../conditions/login');
const { movies_list } = require('../conditions/movies');
const register = require('../conditions/register');


router.post('/register',register.user_register);
router.post('/login', login.user_login);
router.post('/movies', movies_list);
router.post('/logout',);
module.exports = router;