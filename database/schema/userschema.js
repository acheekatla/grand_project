const mongoose = require('mongoose')
const { Schema } = mongoose;

const userSchema = new Schema({
  username:{type:String,required:true},
  password:{type:String,required:true,min:8},
  email:{type:String,required:true,index:{unique:true}},
  phoneno:{type:String,required:true}
});

module.exports ={ userSchema}