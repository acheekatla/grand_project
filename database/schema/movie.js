const mongoose = require("mongoose");
const { Schema } = mongoose;

const movieSchema = new Schema({
  title: { type: String, required: true },
	genre: { type: String },
  release_date: { type: Date, required: true },
});

module.exports = { movieSchema };
