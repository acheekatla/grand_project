const mongoose = require("mongoose")
const { movieSchema } =require("../schema/movie")

const {movie} = mongoose.model('movie', movieSchema);

module.exports ={movie};